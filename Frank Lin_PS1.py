# Name: Frank Lin (1760026)
# ECON 406
# Problem Set 1

import math

amount_deposited = input("Amount Deposited:")
print(amount_deposited)
interest_rate = input("Annual interest rate:")
print(interest_rate)

# Problem 2_1
bill_total_wealth = float(amount_deposited) * (1 + float(interest_rate)) ** 10
print("Bill's total wealth is $", round(bill_total_wealth, 2), sep="")
"""
Using the formula A = P(1+r/n)^nt to solve for A, which is the final amount
"""

# Problem 2_2
time_to_double = math.log(float(amount_deposited) * 2 / float(amount_deposited))/math.log(1 + float(interest_rate))
print("It takes", round(time_to_double, 2), "years to double Bill's money")
"""
Using the formula A = P(1+r/n)^nt to solve for t, which simplified to (1.05)^n = 2, and using log to solve for n
"""